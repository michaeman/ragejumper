RageJumper

RageJumper is our COMP30022 IT Project. It is a game for iPhone implemented in the new swift programming language.
To build this project you will need xcode with swift support.
Current version in use is xcode 6.

Authors:
Michael Liu
Jonathan Gilmour
Stanley Quay
Daniel Edwards
