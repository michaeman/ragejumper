
import SpriteKit

/*
The Main Menu is the initial entry point for the application, and is the
first scene to be rendered by the view.

The main menu contains links to each gameplay mode, as well as links to the
multiplayer menu and options menu.
*/
class OptionsMenu: GameMenuScreen, Temporary {
	private var _labels: Dictionary<String, SKLabelNode> {
		didSet {
			self._labelsAdded = false
		}
	}
	private var parentScene: SKScene
	private var _labelsAdded: Bool
	//private var optionItems: Setting
	internal var labels: Dictionary<String, SKLabelNode> {
		get {
			return self._labels
		}
	}
	
	internal var labelsAdded: Bool {
		get {
			return self.labelsAdded
		}
	}
	
	// Initialisers
	internal convenience init() {
		self.init(origin: nil,backgroundColour: UIColor(r: 0, g: 0, b: 0, a: 255), headerText: "Options", description: "", size: CGSize(width: 1, height: 1))
	}
	
	internal init(origin: SKScene?, backgroundColour colour: UIColor, headerText header: String, description:String, size: CGSize) {
		if(origin == nil) { self.parentScene = SKScene() }
		else { self.parentScene = origin!}
		self._labels = Dictionary<String, SKLabelNode>()
		self._labelsAdded = false
		super.init(colour: colour, header: header, description: description, size: size)
		self.addAllLabels()
	}
	
	internal required init(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	///////////////
	
	override func addLabel(newLabel lb: SKLabelNode, forKey k: String) {
		self._labels[k] = lb
	}
	
	private func addAllLabels() {
		let font = self.defaults.font
		
		self._labels = Dictionary<String, SKLabelNode>()
		
		var menuLabels = [SKLabelNode]()
		
		menuLabels.append(SKLabelNode(text: self.headerLabel))
		menuLabels.append(SKLabelNode(text: "Character Settings"))
		menuLabels.append(SKLabelNode(text: "Sound Settings"))
		menuLabels.append(SKLabelNode(text: "Game Settings"))
		menuLabels.append(SKLabelNode(text: "Back"))
		
		for lb in menuLabels {
			lb.fontName = font
			self.addLabel(newLabel: lb, forKey: lb.text.stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil))
		}
	}
	
	private func setLabelLayout(view: SKView) {
		if(self._labels.count < 1) {
			fatalError("Initialise labels prior to positioning them.")
		}
		
		var screenWidth = view.bounds.size.width
		var screenHeight = view.bounds.size.height
		
		println("TRhis \(screenWidth) and \(screenHeight)")
		
		self._labels[self.headerLabel]!.position = CGPoint(x: screenWidth/2, y: 3*(screenHeight/4)+(screenHeight/6)*0.5)
		self._labels["Character_Settings"]!.position = CGPoint(x: screenWidth/2, y: 3*(screenHeight/4)-(screenHeight/6))
		self._labels["Sound_Settings"]!.position = CGPoint(x: screenWidth/2, y: screenHeight/2-(screenHeight/9) + screenHeight/18 + 10)
		self._labels["Game_Settings"]!.position = CGPoint(x: (screenWidth/2), y: screenHeight/2-(screenHeight/9) - screenHeight/18+10)
		self._labels["Back"]!.position = CGPoint(x: (screenWidth/2), y: screenHeight/6)
	}
	
	private func spawnChildren() {
		if(!self._labelsAdded) {
			for (key,label) in self._labels {
				self.addChild(label)
			}
			self._labelsAdded = true
		}
	}
	
	/*
	didMoveToView() is called upon render. Any visual elements should
	be displayed using this method.
	*/
	override func didMoveToView(view: SKView) {
		//Display all labels
		self.setLabelLayout(view)
		self.spawnChildren()
	}
	
	override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
		// Fill in here
	}
	
	override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
		if(touches.count > 1) {
			return
		}
        
        for touch: AnyObject in touches {
            
            let location = touch.locationInNode(self)
            if self.nodeAtPoint(location) == self.labels["Back"]{
                
                var scene = MainMenu()
                let skView = self.view! as SKView
                let transition = SKTransition.revealWithDirection(SKTransitionDirection.Left, duration: 1.0)
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene, transition: transition)
            }
        }
		
//		for touch in touches {
//			let touchedNode = self.nodeAtPoint(touch.locationInView(self.view))
//			
//			if(touchedNode == self) {
//				return
//			}
//			
//			if(touchedNode == self.labels["Back"]) {
//				self.removeFromView()
//			}
//		}
	}
	
	override func update(currentTime: CFTimeInterval) {
		// Fill in here
	}
	
	internal override func removeFromView() {
		self.view?.presentScene(parentScene)
	}
}
