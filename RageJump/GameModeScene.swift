
import SpriteKit

/*
	GameModeScene objects are where actual gameplay takes place.
	
	Contained inside are platforms, players, etc. and rules that dictate
	the goal platform and other characteristics.

	StaticScreens should segue into a GameModeScene instance, such as
	when displaying a "tap to begin" static screen.
*/
class GameModeScene: SKScene {
	private var _goalPlatformNumber: UInt16
	private var _platforms: [GameShapePlatform]
	private var _numberOfPlatformsToGoal: UInt16
	private var _players: [GameShapeCharacter]
	private var _labels: Dictionary<String, SKLabelNode>
	//YTBI: private var items
	//YTBI: private var enemies
	
	// Getter/Setter indirection
	internal var goalPlatformNumber: UInt16 {
		get {
			return self._goalPlatformNumber
		}
		set(value) {
			if(value > 0) {
				// Platform 0 is the base, cannot set it as victory platform
				self._goalPlatformNumber = value
			}
		}
	}
	internal var numberOfPlatformsToGoal: UInt16 {
		get {
			return self._numberOfPlatformsToGoal
		}
		set(value) {
			if(value > 0) {
				// Must be at least one platform left until the goal is reached
				self._numberOfPlatformsToGoal = value
			}
		}
	}
	internal var platforms: [GameShapePlatform] {
		get {
			return self._platforms
		}
	}
	internal var players: [GameShapeCharacter] {
		get {
			return self._players
		}
	}
	internal var labels: Dictionary<String, SKLabelNode> {
		get {
			return self._labels
		}
	}
	//////////////////////////////
	
	// Initialisers
	internal override init() {
		self._goalPlatformNumber = 16
		self._platforms = [GameShapePlatform]()
		self._numberOfPlatformsToGoal = self._goalPlatformNumber
		self._players = [GameShapeCharacter]()
		self._labels = Dictionary<String, SKLabelNode>()
		
		super.init()
		
		if(self.view == nil) {
			NSException(name: "GameModeScene: Nil View Exception", reason: "Ensure property 'view' has been properly initialised", userInfo: nil).raise()
		}
		
		let width:CGFloat = self.view!.bounds.size.width
		let height:CGFloat = self.view!.bounds.size.height
		let basePlatformSize:CGFloat = 10.0
		var baselineX:CGFloat = -width/2.0
		var baselineY:CGFloat = -(height/2.0) + basePlatformSize
		
		// Always have at least one character, and always a base platform
		self._players.append(GameShapeCharacter())
		self._platforms.append(GameShapePlatform(rectangle: CGRect(x: baselineX, y: baselineY, width: width, height: height)))
	}
	
	internal convenience init(numOfPlatforms plats: UInt16) {
		self.init()
		self._goalPlatformNumber = plats
	}

	required init(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	////////////////////////////////
	
	// Modification of elements
	internal func addPlatform(newPlatform pl:GameShapePlatform) {
		if(self._platforms.count >= 255) {
			println("Warning: Cannot add more platforms. Max (255) reached")
		}
		else {
			self._platforms.append(pl)
		}
	}
	internal func addPlayer(newPlayer pl:GameShapeCharacter) {
		if(self._players.count >= 4) {
			println("Warning: Cannot add more players. Max (4) reached")
		}
		else {
			self._players.append(pl)
		}
	}
	internal func addLabel(newLabel lb:SKLabelNode, forKey k:String) {
		self._labels.updateValue(lb, forKey: k)
	}
	
	internal func removePlatform() {
		self._platforms.removeLast()
	}
	
	internal func removePlatform(atIndex i:Int) {
		self._platforms.removeAtIndex(i)
	}
	
	internal func removePlayer() {
		self._players.removeLast()
	}
	
	internal func removePlayer(atIndex i:Int) {
		self._players.removeAtIndex(i)
	}
	
	internal func removeLabel(forKey key:String) {
		self._labels.removeValueForKey(key)
	}
	///////////////////////
	
	
	override func didMoveToView(view: SKView){
		fatalError("didMoveToView not yet implemented")
	}
	
	override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
		fatalError("touchesBegan not yet implemented")
	}
	
	override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
		fatalError("touchesEnded not yet implemented")
	}
	
	override func update(currentTime: NSTimeInterval) {
		fatalError("update not yet implemented")
	}
}
