//
//  GameViewController.swift
//  SkipGame
//
//  Created by Michael on 11/08/2014.
//  Copyright (c) 2014 Michael Liu. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveFromFile(file : NSString) -> SKNode? {
        
        let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks")
        
        var sceneData = NSData.dataWithContentsOfFile(path!, options: .DataReadingMappedIfSafe, error: nil)
        var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
        
        archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
        let mainMenu = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as MainMenu
        archiver.finishDecoding()
        return mainMenu
    }
}

/*
	Ragejumper uses a single view controller for the entire operation of the application.
	A view is required to display scenes in SpriteKit, and an initial scene (the Main Menu) 
	is rendered first.

	All other SKScene instances, such as splash screens, gameplay screens, and others such as
	option menus are all rendered in the initial view, controlled by this view controller.
*/
class GameViewController: UIViewController {
	
	// Initial entry point for app
    override func viewDidLoad() {
        super.viewDidLoad()
		
		// Apply changes to the view
		let skView = self.view as SKView
		skView.showsFPS = true
		skView.showsNodeCount = true
		skView.ignoresSiblingOrder = true
		////////////////////////////
		
		// Build the main menu
		let mainMenu = MainMenu(backgroundColour: UIColor.blackColor(), headerText: "RageJump", description: "", size: skView.bounds.size)
		mainMenu.scaleMode = .AspectFill
		//////////////////////////////////
		
		// Render the main menu
		skView.presentScene(mainMenu)
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.toRaw())
        } else {
            return Int(UIInterfaceOrientationMask.All.toRaw())
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
