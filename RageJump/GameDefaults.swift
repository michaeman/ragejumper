//
//  Defaults.swift
//  RageJump
//
//  Created by Jonathan Gilmour on 10/09/2014.
//  Copyright (c) 2014 Michael Liu. All rights reserved.
//

import Foundation
import UIKit
/* 
	This module inports default settings from a plist file and allows them to be imported and used in any class
*/
public class GameDefaults {
	let defaultFileName = "Defaults.plist"
	
	private var _file:NSDictionary?
	private var _font:String?
	private var _fontSize:Int?
	private var _platformSize:CGFloat?
	private var _characterType:String?
	
	// Getter/Setter Indirection
	private var file:NSDictionary? {
		get {
			return self._file
		}
	}
	public var font: String? {
		get {
			return self._font
		}
	}
	public var fontSize: Int? {
		get {
			return self._fontSize
		}
	}
	public var platformSize: CGFloat? {
		get {
			return self._platformSize
		}
	}
	public var characterType: String? {
		get {
			return self._characterType
		}
	}
	/////////////////////////////
	
	internal convenience init() {
		self.init(defaultCreate: true)
	}
	
	internal convenience init(defaultCreate:Bool) {
		self.init(fileForPath:"Defaults")
	}
	
	internal init(fileForPath path:String) {
		self._file = NSDictionary()
		
		// Set default fallback values
		self._font = "Helvetica Neue"
		self._fontSize = 10
		self._platformSize = 5.0
		//////////////////////////////
		
		// Attempt to read plist file
		if let plistDict = self.readContentsOfPlist(forPath: path) {
			println("Loading property list file: \"\(path)\"")
			self._file = plistDict
			
			if let font = self.file!.objectForKey("Font") as? String {
				self._font = font
			}
			else {
				println("No field found for Font in file \(path)")
			}
		}
		else {
			println("Property list file \"\(path)\" does not exist")
		}
		/////////////////////////////
	}

	// Takes a path to a .plist file and returns the contents as a dictionary
	private func readContentsOfPlist(forPath path:String) -> Dictionary<String,AnyObject!>? {
		var fail : NSError?
		
		if let fqPath = NSBundle.mainBundle().pathForResource(path, ofType: "plist") {
			// Try to read the file
			let data : NSData! = NSData.dataWithContentsOfFile(fqPath, options: NSDataReadingOptions.DataReadingUncached, error: &fail)
			if let theError = fail {
				// Couldn't find file
				return nil
			}
			// Build the property list
			let pListData : AnyObject! = NSPropertyListSerialization.propertyListWithData(data, options: 0,format: nil, error: &fail)
			if let theError = fail {
				// Couldn't read file
				return nil
			}
			if let baseDict = pListData as? NSDictionary {
				// Create a dict to store everything in
				var plist: Dictionary<String,AnyObject!> = Dictionary<String,AnyObject!>()
				
				for key: AnyObject in baseDict.allKeys {
					// Grab the key for the current pair
					let stringKey : String = key as String
					
					// Add the corresponding value if it exists
					if let value: AnyObject = baseDict.valueForKey(stringKey){
						plist[stringKey] = value
					}
				}
				// Success, return the plist dictionary
				return plist
			}
			else {
				// Conversion to NSDictionary failed
				return nil
			}
		}
		println("die")
		return nil
	}
}