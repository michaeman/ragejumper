//
//  GameWrapper.swift
//  RageJump
//
//  Created by Jonathan Gilmour on 11/09/2014.
//  Copyright (c) 2014 Michael Liu. All rights reserved.
//

import Foundation
import UIKit

// Set of convenience wrappers and types for game entities

extension UIColor {
	// Initialise with colours numbered 0-255
	internal convenience init(r: Int, g: Int, b: Int, a: Int) {
		let red:CGFloat = CGFloat(r)/255
		let green:CGFloat = CGFloat(g)/255
		let blue:CGFloat = CGFloat(b)/255
		let alpha:CGFloat = CGFloat(a)/255
		self.init(red: red, green: green, blue: blue, alpha: alpha)
	}
}

/*
Allows the storage of a maximum and minimum value
Example uses include a limit for a quantity of objects, such as players or levels
*/
public struct MinMax {
	private var _min:Int
	private var _max:Int
	
	public var min:Int {
		get {
			return self._min
		}
		set(value) {
			if(value > self.max) { self.mismatchException(value,maxValue:self.max).raise() }
			self._min = value
		}
	}
	public var max:Int {
		get {
			return self._max
		}
		set(value) {
			if(value < self.min) { self.mismatchException(self.min,maxValue:value).raise() }
			self._max = value
		}
	}
	
	public init(minValue:Int, maxValue:Int) {
		self._min = minValue
		self._max = maxValue
		if(self.min > self.max) { self.mismatchException(minValue,maxValue:maxValue).raise() }
	}
	
	private func mismatchException(minValue:Int,maxValue:Int) -> NSException {
		return NSException(name: "MinMax_MismatchError", reason: "Attempt to create instance of GameMode with player range (\(minValue),\(maxValue))", userInfo: nil)
	}
}