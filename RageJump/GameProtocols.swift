//
//  GameProtocols.swift
//  RageJump
//
//  Created by Jonathan Gilmour on 10/09/2014.
//  Copyright (c) 2014 Michael Liu. All rights reserved.
//

import Foundation

/* 
	Classes that implement the Timed protocol must incorporate a timer that will update them in some way after a certain amount of time.
	
	For example, a game's start screen may disappear after a few seconds, and thus its update method should remove it from view after this
*/
protocol Timed {
	func updateTimedObject(#timer:NSTimer)
}

/*
	Classes that implement the Temporary protocol must only reside on the screen for a limited time. 

	For example, a menu screen will be removed once an option is selected.
*/
protocol Temporary {
	func removeFromView()
}

/*
	Sets the default characteristics any character must employ
*/
protocol Character {
	var fullName:String { get set }
}