
import Foundation
import SpriteKit

/*
	An in-game object that utilises textures
*/
internal class GameSprite : SKSpriteNode {
	private var defaults: GameDefaults
	
	internal override init() {
		self.defaults = GameDefaults(defaultCreate: true)
		super.init()
	}
	
	internal required init(coder aDecoder: NSCoder) {
		self.defaults = GameDefaults(defaultCreate: true)
		super.init(coder: aDecoder)
	}
}

/*
	An in-game object that utilises shapes
*/
internal class GameShape : SKShapeNode {
	private let defaults: GameDefaults
	
	// Initialisers
	internal override init() {
		self.defaults = GameDefaults(defaultCreate: true)
		super.init()
	}
	
	internal init(createDefaults defaultOn:Bool) {
		self.defaults = GameDefaults(defaultCreate: defaultOn)
		super.init()
	}
	
	internal init(path:CGPathRef) {
		self.defaults = GameDefaults(defaultCreate: true)
		super.init()
		self.path = path
	}
	
	internal required init(coder aDecoder: NSCoder) {
		self.defaults = GameDefaults(defaultCreate: true)
		super.init(coder: aDecoder)
	}
	
	internal convenience init(circleOfRadius radius:CGFloat) {
		var circle: CGMutablePathRef = CGPathCreateMutable()
		CGPathAddArc(circle, nil, 0.0, 0.0, radius, 0.0, CGFloat(M_PI) * 2.0, true)
		self.init(path:circle)
	}
	
	internal convenience init(rectangle:CGRect) {
		var rect: CGPathRef = CGPathCreateWithRect(rectangle, nil)
		self.init(path:rect)
	}
	
	internal convenience init(roundedRect:CGRect, cornerW:CGFloat, cornerH:CGFloat) {
		var rrect: CGPathRef = CGPathCreateWithRoundedRect(roundedRect, cornerW, cornerH, nil)
		self.init(path:rrect)
	}
	/////////////////
	
	
}

/*
	An in-game platform formed from a shape
*/
internal class GameShapePlatform : GameShape {
	private var _unsolid: Bool
	internal var unsolid: Bool {
		get {
			return self._unsolid
		}
		set(value) {
			self._unsolid = value
		}
	}
	
	// Initialisers
	internal override init() {
		self._unsolid = false
		super.init()
	}
	
	internal override init(path:CGPathRef) {
		self._unsolid = false
		super.init(path:path)
	}
	
	internal convenience init(circleOfRadius radius:CGFloat) {
		var circle: CGMutablePathRef = CGPathCreateMutable()
		CGPathAddArc(circle, nil, 0.0, 0.0, radius, 0.0, CGFloat(M_PI) * 2.0, true)
		self.init(path:circle)
	}
	
	internal convenience init(rectangle:CGRect) {
		var rect: CGPathRef = CGPathCreateWithRect(rectangle, nil)
		self.init(path:rect)
	}

	internal required init(coder aDecoder: NSCoder) {
		self._unsolid = false
	    super.init(coder: aDecoder)
	}
	///////////////
	
	
}

/*
	An in-game platform formed from a sprite
*/
internal class GameSpritePlatform : GameSprite {
	private var _unsolid: Bool
	internal var unsolid: Bool {
		get {
			return self._unsolid
		}
		set(value) {
			self._unsolid = value
		}
	}
	
	// Initialisers
	internal override init() {
		self._unsolid = false
		super.init()
	}
	
	internal required init(coder aDecoder: NSCoder) {
		self._unsolid = false
		super.init(coder: aDecoder)
	}
	///////////////
	
	
}

/*
	An in-game character (player, enemy, etc.) formed from a shape
*/
internal class GameShapeCharacter : GameShape, Character {
	private var _fullName: String
	internal var fullName: String {
		get {
			return self._fullName
		}
		set(value) {
			self._fullName = value
		}
	}
	
	// Initialisers
	internal init(characterName name:String) {
		self._fullName = name
		super.init()
	}
	
	// Initialisers
	internal override init() {
		self._fullName = "Jon Rager"
		super.init()
	}
	
	internal required init(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	///////////////
	
	
}