
import Foundation
import SpriteKit

/*
	This class sets out everything a game mode should implement at the basic level.

	Subclass GameMode to create your own game modes with specific rules and implementations.
*/
public class GameMode {
	private var defaults: GameDefaults
	private var _name:String
	private var _multiplayer:Bool
	private var _description:String
	private var _playerNumber:MinMax
	private var _welcomeScreen:GameSplashScreen
	private var _gameScene:GameModeScene
	
	// Getter/Setter indirection
	public var name:String {
		get {
			return self._name
		}
		set(value) {
			self._name = value
		}
	}
	public var description:String {
		get {
			return self._description
		}
		set(value) {
			self._description = value
		}
	}
	public var multiplayer:Bool {
		get {
			return self._multiplayer
		}
	}
	public var playerNumber:MinMax {
		get {
			// Return a copy, not the object itself
			return MinMax(minValue: _playerNumber.min, maxValue: _playerNumber.max)
		}
		set(value) {
			self._playerNumber = MinMax(minValue: value.min, maxValue: value.max)
		}
	}
	public var welcomeScreen:GameSplashScreen {
		get {
			// Return a copy, not the object itself
			return GameSplashScreen(colour: _welcomeScreen.backgroundColor, header: _welcomeScreen.headerLabel, description: _welcomeScreen.descriptionLabel, size: _welcomeScreen.size)
		}
		set(value) {
			// Set a copy
			self._welcomeScreen = GameSplashScreen(colour: value.backgroundColor, header: value.headerLabel, description: value.descriptionLabel, size: value.size)
		}
	}
	//////////////////////////////
	
	// Initialisers
	public init() {
		self.defaults = GameDefaults()
		self._name = "Game Mode"
		self._description = "A basic game mode description"
		self._multiplayer = false
		self._playerNumber = MinMax(minValue: 1, maxValue: 1)
		self._welcomeScreen = GameSplashScreen()
		self._gameScene = GameModeScene()
	}
	
	public convenience init(modeName mode:String) {
		self.init()
		self._name = mode
	}
	
	public convenience init(modeName mode:String, modeDescription desc:String) {
		self.init(modeName: mode)
		self._description = desc
	}
	
	public convenience init(modeName mode:String, modeDescription desc:String, minMaxPlayers minmax:MinMax) {
		self.init(modeName: mode, modeDescription: desc)
		self._playerNumber = MinMax(minValue: minmax.min, maxValue: minmax.max)
	}
	
	public convenience init(modeName mode:String, modeDescription desc:String, minPlayers min:Int, maxPlayers max:Int) {
		self.init(modeName: mode, modeDescription: desc)
		self._playerNumber = MinMax(minValue: min, maxValue: max)
	}	
	
	public convenience init(modeName mode:String, modeDescription desc:String, minPlayers min:Int, maxPlayers max:Int, multiplayerOn multi:Bool) {
		self.init(modeName: mode, modeDescription: desc, minPlayers:min, maxPlayers:max)
		self._multiplayer = multi
	}
	////////////////
	
}

