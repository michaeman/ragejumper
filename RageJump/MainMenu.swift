
import SpriteKit

/*
	The Main Menu is the initial entry point for the application, and is the
	first scene to be rendered by the view.

	The main menu contains links to each gameplay mode, as well as links to the
	multiplayer menu and options menu.
*/
class MainMenu: GameMenuScreen {
	private let options:OptionsMenu
	private var _labels: Dictionary<String, SKLabelNode> {
		didSet {
			self._labelsAdded = false
		}
	}
	private var _labelsAdded: Bool
	//private var optionItems: Setting
	internal var labels: Dictionary<String, SKLabelNode> {
		get {
			return self._labels
		}
	}
	
	internal var labelsAdded: Bool {
		get {
			return self.labelsAdded
		}
	}
	////////////////////////////
	
	// Initialisers
	internal convenience init() {
		self.init(backgroundColour: UIColor(r: 0, g: 0, b: 0, a: 255), headerText: "RageJumper", description: "", size: CGSize(width: 1, height: 1))
	}
	
	internal init(backgroundColour colour: UIColor, headerText header: String, description:String, size: CGSize) {
		self.options = OptionsMenu()
		self._labels = Dictionary<String, SKLabelNode>()
		self._labelsAdded = false
		super.init(colour: colour, header: header, description: description, size: size)
		self.addAllLabels()
		self.options = OptionsMenu(origin: self, backgroundColour: colour, headerText: "Options", description: "", size: size)
		self.options.scaleMode = self.scaleMode
	}
	
	internal required init(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	///////////////
	
	override func addLabel(newLabel lb: SKLabelNode, forKey k: String) {
		_labels[k] = lb
	}
	
	private func addAllLabels() {
		let font = self.defaults.font
		
		self._labels = Dictionary<String, SKLabelNode>()
		
		var menuLabels = [SKLabelNode]()
		
		menuLabels.append(SKLabelNode(text: self.headerLabel))
		menuLabels.append(SKLabelNode(text: "Rush"))
		menuLabels.append(SKLabelNode(text: "Arcade"))
		menuLabels.append(SKLabelNode(text: "Classic"))
		menuLabels.append(SKLabelNode(text: "Multiplayer"))
		menuLabels.append(SKLabelNode(text: "Options"))
        menuLabels.append(SKLabelNode(text: "About"))
		
		for lb in menuLabels {
			lb.fontName = font
			self.addLabel(newLabel: lb, forKey: lb.text.stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil))
		}
	}
	
	private func setLabelLayout(view: SKView) {
		if(self.labels.count < 1) {
			fatalError("Initialise labels prior to positioning them.")
		}
		
		var screenWidth = view.bounds.size.width
		var screenHeight = view.bounds.size.height
		
		self._labels[self.headerLabel]!.position = CGPoint(x: screenWidth/2, y: 3*(screenHeight/4)+(screenHeight/6)*0.5)
		self._labels["Rush"]!.position = CGPoint(x: screenWidth/4, y: 3*(screenHeight/4)-(screenHeight/6))
		self._labels["Arcade"]!.position = CGPoint(x: screenWidth/2, y: 3*(screenHeight/4)-(screenHeight/6))
		self._labels["Classic"]!.position = CGPoint(x: 3*(screenWidth/4), y: 3*(screenHeight/4)-(screenHeight/6))
		self._labels["Multiplayer"]!.position = CGPoint(x: screenWidth/2, y: screenHeight/2-(screenHeight/9))
		self._labels["Options"]!.position = CGPoint(x: screenWidth/2, y: screenHeight/4-(screenHeight/9)*0.5)
        self._labels["About"]!.position = CGPoint(x: screenWidth*3/4, y: screenHeight/4-(screenHeight/9)*0.5)
	}
	
	private func spawnChildren() {
		if(!self._labelsAdded) {
			for (key,label) in self._labels {
				self.addChild(label)
			}
			self._labelsAdded = true
		}
	}
	
	private func moveTo(Scene sc:SKScene) {
		// To prevent disallowed access, may want to have some sort of permissions check in here
		self.view?.presentScene(sc, transition: nil)
	}
	
	/*
		didMoveToView() is called upon render. Any visual elements should
		be displayed using this method.
	*/
    override func didMoveToView(view: SKView) {
		//Display all labels
		self.setLabelLayout(view)
		self.spawnChildren()
    }
	
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
		
    }
	
	override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
		if(touches.count > 1) {
			return
		}
		
        for touch: AnyObject in touches {
            
            let location = touch.locationInNode(self)
            if self.nodeAtPoint(location) == self.labels["Options"]{
                
                var scene = OptionsMenu()
                let skView = self.view! as SKView
                let transition = SKTransition.revealWithDirection(SKTransitionDirection.Left, duration: 1.0)
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene, transition: transition)
            }
            
            if self.nodeAtPoint(location) == self.labels["Multiplayer"]{
                
                var scene = MultiMenu()
                let skView = self.view! as SKView
                let transition = SKTransition.revealWithDirection(SKTransitionDirection.Left, duration: 1.0)
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene, transition: transition)
            }
            
            if self.nodeAtPoint(location) == self.labels["RushMode"]{
                
                var scene = RushMenu()
                let skView = self.view! as SKView
                let transition = SKTransition.revealWithDirection(SKTransitionDirection.Left, duration: 1.0)
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene, transition: transition)
            }
            
            if self.nodeAtPoint(location) == self.labels["Classic"]{
                
                var scene = ClassicMenu()
                let skView = self.view! as SKView
                let transition = SKTransition.revealWithDirection(SKTransitionDirection.Left, duration: 1.0)
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene, transition: transition)
            }
            
            if self.nodeAtPoint(location) == self.labels["About"]{
                
                var scene = AboutMenu()
                let skView = self.view! as SKView
                let transition = SKTransition.revealWithDirection(SKTransitionDirection.Left, duration: 1.0)
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene, transition: transition)
            }
            
//			let touchedNode = self.nodeAtPoint(touch.locationInView(self.view))
//			
//			if(touchedNode == self) {
//				return
//			}
//			
//			if(touchedNode == self.labels["Options"]) {
//				self.moveTo(Scene: self.options)
//			}
		}
	}
    
    override func update(currentTime: CFTimeInterval) {
		// Fill in here
    }
}
