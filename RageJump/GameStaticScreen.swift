
import Foundation
import SpriteKit

/*
	GameScreens are scenes where no actual gameplay takes place.

	A main menu or splash screen is an example of a GameScreen.
*/
public class GameScreen: SKScene {
	private var _defaults:GameDefaults
	private var _headerLabel: SKLabelNode
	private var _descriptionLabel: SKLabelNode
	
	// Getter/Setter indirection
	internal var headerLabel:String {
		get {
			return self._headerLabel.text
		}
		set(value) {
			self._headerLabel.text = value
		}
	}
	internal var descriptionLabel:String {
		get {
			return self._descriptionLabel.text
		}
		set(value) {
			self._descriptionLabel.text = value;
		}
	}
	internal var defaults:GameDefaults {
		get {
			return self._defaults
		}
	}
	//////////////////////////////
	
	// Initialisers
	internal convenience override init() {
		self.init(colour: UIColor(r: 255, g: 255, b: 255, a: 255),header: "#Placeholder#",description: "",size: CGSize(width: 1, height: 1))
	}
	
	internal init(colour:UIColor,header:String,description:String, size:CGSize) {
		_defaults = GameDefaults()
		_headerLabel = SKLabelNode()
		_headerLabel.text = header
		_descriptionLabel = SKLabelNode()
		_descriptionLabel = SKLabelNode()
		_descriptionLabel.text = description
		
		super.init(size: size)
		self.backgroundColor = colour
	}
	
	public required init(coder aDecoder: NSCoder) {
		_defaults = GameDefaults()
		_headerLabel = SKLabelNode()
		_headerLabel.text = ""
		_descriptionLabel = SKLabelNode()
		_descriptionLabel = SKLabelNode()
		_descriptionLabel.text = ""
		
		super.init(coder: aDecoder)
	}
	///////////////
	
	
	
}

/*
	Defines a menu screen that contains options

	As this screen employs the "Temporary" protocol, it must be removable from view.

	The screen may be removed when moving to a different screen, or if it is simply 
	taken away, revealing what is underneath (such as a pause screen).

	A menu is different from a splash screen in that it must be explicitly commanded 
	to be removed by the user or some other entity (such as tapping or shaking).
*/
public class GameMenuScreen:GameScreen,Temporary {
	////////////////////////////
	
	// Initialisers
	internal convenience init() {
		self.init(colour: UIColor(r: 255, g: 255, b: 255, a: 255),header: "#Placeholder#",description: "", size: CGSize(width: 1, height: 1))
	}
	internal override init(colour:UIColor,header:String,description:String="",size:CGSize) {
		super.init(colour: colour, header: header, description: description,size:size)
	}

	public required init(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	///////////////
	
	internal func addLabel(newLabel lb:SKLabelNode, forKey k:String) {
		fatalError("addLabel(newLabel: forKey:) must be implemented")
	}
	
	internal func removeLabel(forKey key:String) {
		fatalError("removeLabel(forKey:) must be implemented")
	}
	
	internal func removeFromView() {
		fatalError("removeFromView has not been implemented")
	}
}

/*
	Defines a screen that purely displays information

	PROTOCOLS
	- As this screen employs the "Temporary" protocol, it must be removable from view.
	- As this screen employs the "Timed" protocol, it must update after a constant amount of time.

	REQUIRED ACTIONS
	- The screen may be removed when moving to a different screen, or if it is simply taken away, revealing what is underneath (such as a countdown screen).
	- A splash screen must be able to be removed either by a timer, or an interaction by the user, such as tapping anywhere on screen.
*/
public class GameSplashScreen:GameScreen,Temporary,Timed {
	
	// Initialisers
	public override init(colour:UIColor,header:String,description:String, size:CGSize) {
		super.init(colour: colour, header: header, description: description,size:size)
	}

	public required init(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}
	///////////////
	
	internal func removeFromView() {
		fatalError("removeFromView has not been implemented")
	}
	
	internal func updateTimedObject(#timer:NSTimer) {
		fatalError("updateTimedObject has not been implemented")
	}
}










