//
//  GameScene.swift
//  SkipGame
//
//  Created by Michael on 11/08/2014.
//  Copyright (c) 2014 Michael Liu. All rights reserved.
//

import SpriteKit

class MultiplayerView: SKScene {
    
    let classic = SKSpriteNode(imageNamed: "Classic")
    let rush = SKSpriteNode(imageNamed: "Rush")
    let arcade = SKSpriteNode(imageNamed: "Arcade")
    let more = SKSpriteNode(imageNamed: "More")
    let single = SKSpriteNode(imageNamed: "Single")
    
    
    override func didMoveToView(view: SKView) {
        
        self.backgroundColor = UIColor.blackColor()
        
        self.classic.position = CGPointMake(CGRectGetMidX(self.frame) - 150, CGRectGetMidY(self.frame) + 50)
        self.addChild(self.classic)
        
        self.arcade.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) + 50)
        self.addChild(self.arcade)
        
        self.rush.position = CGPointMake(CGRectGetMidX(self.frame) + 150, CGRectGetMidY(self.frame) + 50)
        self.addChild(self.rush)
        
        self.more.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) - 100)
        self.addChild(self.more)
        
        self.single.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame) - 30)
        self.addChild(self.single)
        
        
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
//        /* Called when a touch begins */
//        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            if self.nodeAtPoint(location) == self.single{

                var scene = MainMenu()
                let skView = self.view! as SKView
                let transition = SKTransition.revealWithDirection(SKTransitionDirection.Left, duration: 1.0)
                skView.ignoresSiblingOrder = true
                scene.scaleMode = .ResizeFill
                scene.size = skView.bounds.size
                skView.presentScene(scene, transition: transition)
            }
//
//            if self.nodeAtPoint(location) == self.arcade{
//                
//                var scene = ArcadeMode(size: self.size)
//                let skView = self.view as SKView
//                skView.ignoresSiblingOrder = true
//                scene.scaleMode = .ResizeFill
//                scene.size = skView.bounds.size
//                skView.presentScene(scene)
//                
//            }
//            
//            if self.nodeAtPoint(location) == self.rush{
//                
//                var scene = RushMode(size: self.size)
//                let skView = self.view as SKView
//                skView.ignoresSiblingOrder = true
//                scene.scaleMode = .ResizeFill
//                scene.size = skView.bounds.size
//                skView.presentScene(scene)
//                
//            }
//            
//            if self.nodeAtPoint(location) == self.single{
//                
//                var scene = GameScene(size: self.size)
//                let skView = self.view as SKView
//                skView.ignoresSiblingOrder = true
//                scene.scaleMode = .ResizeFill
//                scene.size = skView.bounds.size
//                skView.presentScene(scene)
//                
//            }
            
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
